# WikidataStream

An eye-catching stream of Wikidata edits.

## URL

https://tobias47n9e.gitlab.io/wikidata-stream/

## Sources

- Gear icon: https://commons.wikimedia.org/wiki/File:Gear_icon-72a7cf.svg
- Wikidata logo: https://commons.wikimedia.org/wiki/File:Wikidata-logo-en.svg
- Bug icon: https://commons.wikimedia.org/wiki/File:Ic_bug_report_48px.svg
- Pause icon: https://commons.wikimedia.org/wiki/File:Ic_pause_48px.svg
- Play icon: https://commons.wikimedia.org/wiki/File:Ic_play_arrow_48px.svg

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
