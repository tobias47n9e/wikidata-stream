import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SitelinkChangesComponent } from './sitelink-changes.component';

describe('SitelinkChangesComponent', () => {
  let component: SitelinkChangesComponent;
  let fixture: ComponentFixture<SitelinkChangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SitelinkChangesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SitelinkChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
