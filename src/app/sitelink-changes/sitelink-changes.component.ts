import { Component, Input, OnInit } from '@angular/core';
import { Entity } from 'wikidata-sdk';

@Component({
  selector: 'app-sitelink-changes',
  templateUrl: './sitelink-changes.component.html',
  styleUrls: ['./sitelink-changes.component.scss'],
})
export class SitelinkChangesComponent implements OnInit {
  @Input()
  item: Entity;

  @Input()
  oldRevision: Entity;

  get removedSitelinks() {
    const newKeys = Object.keys(this.item.sitelinks);
    const oldKeys = Object.keys(this.oldRevision.sitelinks);

    return oldKeys
      .filter(sitelinkLocale => !newKeys.includes(sitelinkLocale))
      .map(locale => this.oldRevision.sitelinks[locale]);
  }

  get addedSitelinks() {
    const newKeys = Object.keys(this.item.sitelinks);
    const oldKeys = Object.keys(this.oldRevision.sitelinks);

    return newKeys
      .filter(sitelinkLocale => !oldKeys.includes(sitelinkLocale))
      .map(locale => this.item.sitelinks[locale]);
  }

  constructor() {}

  ngOnInit() {}
}
