import { Component, Input, OnInit } from '@angular/core';
import md5 from 'md5';
import { Entity } from 'wikidata-sdk';

@Component({
  selector: 'app-item-image',
  templateUrl: './item-image.component.html',
  styleUrls: ['./item-image.component.scss'],
})
export class ItemImageComponent implements OnInit {
  private imageUrlPrefix =
    'https://upload.wikimedia.org/wikipedia/commons/thumb';

  @Input()
  item: Entity;

  get imageUrl(): string {
    if (!this.item.claims.P18) {
      return;
    }

    const fileNameRaw: string = <string>(
      this.item.claims.P18[0].mainsnak.datavalue.value
    );

    if (!fileNameRaw) {
      return;
    }

    const fileName = fileNameRaw.replace(/\s/g, '_');
    const fileNameMd5 = md5(fileName);
    const firstChar = fileNameMd5.slice(0, 1);
    const secondChar = fileNameMd5.slice(1, 2);

    return `${this.imageUrlPrefix}/${firstChar}/${firstChar}${secondChar}/${fileName}/300px-${fileName}`;
  }

  ngOnInit() {}
}
