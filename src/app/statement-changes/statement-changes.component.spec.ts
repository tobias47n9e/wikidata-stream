import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatementChangesComponent } from './statement-changes.component';

describe('StatementChangesComponent', () => {
  let component: StatementChangesComponent;
  let fixture: ComponentFixture<StatementChangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatementChangesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
