import { Component, Input, OnInit } from '@angular/core';
import { Claim, ClaimSnakEntityValue, Entity } from 'wikidata-sdk';
import { AngularFluentService } from 'angular-fluent';

@Component({
  selector: 'app-statement-changes',
  templateUrl: './statement-changes.component.html',
  styleUrls: ['./statement-changes.component.scss'],
})
export class StatementChangesComponent implements OnInit {
  @Input()
  item: Entity;

  @Input()
  oldRevision: Entity;

  @Input()
  relatedEntities: any;

  locale: string;

  /**
   * Statements of new properties
   */
  get addedStatements() {
    const newKeys = Object.keys(this.item.claims);
    const oldKeys = Object.keys(this.oldRevision.claims);
    const allKeys = newKeys.concat(oldKeys);
    const addedStatements = {};

    for (const propertyKey of newKeys) {
      if (!oldKeys.includes(propertyKey)) {
        // New property key -> Add all statements
        addedStatements[propertyKey] = this.item.claims[propertyKey];
        continue;
      }
    }

    return addedStatements;
  }

  constructor(private angularFluentService: AngularFluentService) {}

  ngOnInit() {
    this.locale = this.angularFluentService.locale.value;

    this.angularFluentService.locale.subscribe(value => (this.locale = value));
  }

  localizePropertyName(key: string) {
    const fullKey = `Property:${key}`;

    if (Object.keys(this.relatedEntities).includes(fullKey)) {
      return this.relatedEntities[fullKey].labels[this.locale].value;
    }

    return key;
  }

  localizeStatementTarget(statement: Claim) {
    if (statement.mainsnak.datavalue.type === 'wikibase-entityid') {
      const itemValue = <ClaimSnakEntityValue>statement.mainsnak.datavalue;
      const key = itemValue.value.id;
      if (Object.keys(this.relatedEntities).includes(key)) {
        return this.relatedEntities[key].labels[this.locale].value;
      }
    }

    return statement.mainsnak.datavalue.value;
  }
}
