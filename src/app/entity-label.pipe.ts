import { Pipe, PipeTransform } from '@angular/core';
import { WikidataService } from './wikidata.service';
import { AngularFluentService } from 'angular-fluent';

@Pipe({
  name: 'entityLabel',
})
export class EntityLabelPipe implements PipeTransform {
  constructor(
    private wikidataService: WikidataService,
    private angularFluentService: AngularFluentService,
  ) {}

  transform(entityId: string, args?: any): any {
    const locale = this.angularFluentService.locale.value;

    if (this.wikidataService.propertyEntities.hasOwnProperty(entityId)) {
      const property = this.wikidataService.propertyEntities[entityId];

      if (property.labels.hasOwnProperty(locale)) {
        return property.labels[locale].value;
      }
    }

    return entityId;
  }
}
