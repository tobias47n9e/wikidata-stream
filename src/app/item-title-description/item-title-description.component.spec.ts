import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemTitleDescriptionComponent } from './item-title-description.component';

describe('ItemTitleDescriptionComponent', () => {
  let component: ItemTitleDescriptionComponent;
  let fixture: ComponentFixture<ItemTitleDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemTitleDescriptionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemTitleDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
