import { Component, Input, OnInit } from '@angular/core';
import { Entity } from 'wikidata-sdk';

@Component({
  selector: 'app-item-title-description',
  templateUrl: './item-title-description.component.html',
  styleUrls: ['./item-title-description.component.scss'],
})
export class ItemTitleDescriptionComponent implements OnInit {
  @Input()
  item: Entity;

  constructor() {}

  ngOnInit() {}
}
