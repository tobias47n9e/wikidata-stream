import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { StatementComponent } from './statement/statement.component';
import { EntityLabelPipe } from './entity-label.pipe';
import { DiffLinkPipe } from './diff-link.pipe';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { GearIconComponent } from './gear-icon/gear-icon.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StringTransPipe } from './string-trans.pipe';
import { WikiLinkPipe } from './wiki-link.pipe';
import { TargetBlankPipe } from './target-blank.pipe';
import { PauseComponent } from './pause/pause.component';
import {
  AngularFluentModule,
  AngularFluentService,
  FluentPipe,
} from 'angular-fluent';
import { EditTitleComponent } from './edit-title/edit-title.component';
import { EditSummaryComponent } from './edit-summary/edit-summary.component';
import { ItemTitleDescriptionComponent } from './item-title-description/item-title-description.component';
import { ItemImageComponent } from './item-image/item-image.component';
import { LabelChangesComponent } from './label-changes/label-changes.component';
import { DescriptionChangesComponent } from './description-changes/description-changes.component';
import { SitelinkChangesComponent } from './sitelink-changes/sitelink-changes.component';
import { StatementChangesComponent } from './statement-changes/statement-changes.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    StatementComponent,
    EntityLabelPipe,
    DiffLinkPipe,
    SideMenuComponent,
    GearIconComponent,
    StringTransPipe,
    WikiLinkPipe,
    TargetBlankPipe,
    PauseComponent,
    EditTitleComponent,
    EditSummaryComponent,
    ItemTitleDescriptionComponent,
    ItemImageComponent,
    LabelChangesComponent,
    DescriptionChangesComponent,
    SitelinkChangesComponent,
    StatementChangesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFluentModule,
  ],
  providers: [AngularFluentService, FluentPipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
