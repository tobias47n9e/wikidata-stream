import { Component, Input, OnInit } from '@angular/core';
import { Edit } from '../model/edit';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-summary',
  templateUrl: './edit-summary.component.html',
  styleUrls: ['./edit-summary.component.scss'],
})
export class EditSummaryComponent implements OnInit {
  @Input()
  edit: Edit;

  get editTimeStamp() {
    return moment.unix(this.edit.timestamp).toDate();
  }

  constructor() {}

  ngOnInit() {}
}
