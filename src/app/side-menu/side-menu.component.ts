import { Component, OnInit, HostBinding } from "@angular/core";
import { FormControl } from "@angular/forms";
import {AngularFluentService} from "angular-fluent";

@Component({
  selector: "app-side-menu",
  templateUrl: "./side-menu.component.html",
  styleUrls: ["./side-menu.component.scss"]
})
export class SideMenuComponent implements OnInit {
  @HostBinding("class.is-open")
  menuOpen: boolean = false;

  localeControl: FormControl;

  constructor(
    private angularFluentService: AngularFluentService,
  ) {}

  ngOnInit() {
    this.localeControl = new FormControl();
    this.localeControl.setValue("en");
  }

  toggle() {
    this.menuOpen = !this.menuOpen;
  }

  applyAndClose() {
    this.menuOpen = false;
    this.angularFluentService.setLocale(this.localeControl.value);
  }
}
