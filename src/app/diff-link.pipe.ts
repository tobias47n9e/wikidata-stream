import { Pipe, PipeTransform } from "@angular/core";
import { Edit } from "./model/edit";

@Pipe({
  name: "diffLink"
})
export class DiffLinkPipe implements PipeTransform {
  /**
   * Returns a link to an item diff
   *
   * New item: params are ?title=<Q-ID>&oldid=<ID>
   * Edited item: params are ?title=<Q-ID>&type=revision&diff=<NEW_ID>&oldid=<OLD_ID>
   */
  transform(edit: Edit, args?: any): any {
    const base = `${edit.server_url}${edit.server_script_path}/index.php`;
    const params = new URLSearchParams();
    params.set("title", edit.title);
    params.set("oldid", edit.revision.new.toString());

    if (edit.revision.old) {
      params.set("type", "revision");
      params.set("oldid", edit.revision.old.toString());
      params.set("diff", edit.revision.new.toString());
    }

    return `${base}?${params.toString()}`;
  }
}
