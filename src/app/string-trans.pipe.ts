import { Pipe, PipeTransform } from "@angular/core";
import {AngularFluentService, FluentPipe} from "angular-fluent";

@Pipe({
  name: "stringTrans"
})
export class StringTransPipe implements PipeTransform {
  constructor(
    private angularFluentService: AngularFluentService,
    private fluentPipe: FluentPipe,
  ) {}

  transform(value: any, args?: any): any {
    const locale = this.angularFluentService.locale.value;

    if (Object.keys(value).length === 0) {
      return this.fluentPipe.transform('missing');
    }

    if (value[locale]) {
      return value[locale].value;
    }

    for (const navigatorLocale of navigator.languages) {
      const lowerCaseLocale = navigatorLocale.toLowerCase();
      const languageLocale = navigatorLocale.split("-")[0];

      if (value[lowerCaseLocale]) {
        return value[lowerCaseLocale].value;
      }

      if (value[languageLocale]) {
        return value[languageLocale].value;
      }
    }

    if (value["en"]) {
      return value["en"].value;
    }

    const availableLocales = Object.keys(value);
    const randomLocale =
      availableLocales[(availableLocales.length * Math.random()) << 0];

    return value[randomLocale].value;
  }
}
