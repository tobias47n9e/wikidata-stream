import { Injectable, NgZone } from "@angular/core";
import { HttpClient } from "selenium-webdriver/http";
import { ConnectableObservable, Observable, observable } from "rxjs";
import {Edit} from "./model/edit";

@Injectable({
  providedIn: "root"
})
export class StreamService {
  eventSource: any = window["EventSource"];

  constructor(private ngZone: NgZone) {}

  observeStream(): Observable<Edit> {
    return new Observable<Edit>(observable => {
      const eventSource = new this.eventSource(
        "https://stream.wikimedia.org/v2/stream/recentchange"
      );

      eventSource.onmessage = event => {
        let data = JSON.parse(event.data);
        this.ngZone.run(() => observable.next(data));
      };

      return () => eventSource.close();
    });
  }
}
