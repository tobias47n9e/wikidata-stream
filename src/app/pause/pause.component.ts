import { Component, OnInit, Input } from "@angular/core";
import { PauseService } from "../pause.service";

@Component({
  selector: "app-pause",
  templateUrl: "./pause.component.html",
  styleUrls: ["./pause.component.scss"]
})
export class PauseComponent implements OnInit {
  @Input()
  paused: boolean;

  constructor(private pauseService: PauseService) {}

  ngOnInit() {}

  togglePaused() {
    this.pauseService.toggle();
  }
}
