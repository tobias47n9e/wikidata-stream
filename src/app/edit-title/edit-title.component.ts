import { Component, Input, OnInit } from '@angular/core';
import { Edit } from '../model/edit';

@Component({
  selector: 'app-edit-title',
  templateUrl: './edit-title.component.html',
  styleUrls: ['./edit-title.component.scss'],
})
export class EditTitleComponent implements OnInit {
  @Input()
  edit: Edit;

  get itemIsNew() {
    return !this.edit.revision.old;
  }

  constructor() {}

  ngOnInit() {}
}
