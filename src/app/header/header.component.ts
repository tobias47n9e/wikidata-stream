import { Component, OnInit, Input, HostListener } from "@angular/core";
import { SideMenuComponent } from "../side-menu/side-menu.component";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  @Input()
  sideMenu: SideMenuComponent;

  @Input()
  paused: boolean;

  constructor() {}

  ngOnInit() {}
}
