import { Component, Input } from "@angular/core";
import { Claim } from "wikidata-sdk";

@Component({
  selector: "app-statement",
  templateUrl: "./statement.component.html",
  styleUrls: ["./statement.component.scss"]
})
export class StatementComponent {
  @Input()
  statement: Claim;

  constructor() {}
}
