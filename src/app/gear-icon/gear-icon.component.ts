import { Component, OnInit, Input, HostListener } from "@angular/core";
import { SideMenuComponent } from "../side-menu/side-menu.component";

@Component({
  selector: "app-gear-icon",
  templateUrl: "./gear-icon.component.html",
  styleUrls: ["./gear-icon.component.scss"]
})
export class GearIconComponent implements OnInit {
  @Input()
  sideMenu: SideMenuComponent;

  constructor() {}

  ngOnInit() {}

  @HostListener("click")
  toggle() {
    this.sideMenu.toggle();
  }
}
