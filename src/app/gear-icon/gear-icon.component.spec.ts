import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GearIconComponent } from './gear-icon.component';

describe('GearIconComponent', () => {
  let component: GearIconComponent;
  let fixture: ComponentFixture<GearIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GearIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GearIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
