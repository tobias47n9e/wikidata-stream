import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PauseService {
  paused: boolean = false;

  pausedSubject: Subject<boolean> = new Subject<boolean>();

  constructor() {}

  toggle() {
    this.paused = !this.paused;
    this.pausedSubject.next(this.paused);
  }
}
