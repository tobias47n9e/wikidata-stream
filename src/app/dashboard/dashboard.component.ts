import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Edit } from '../model/edit';
import { Entity } from 'wikidata-sdk';
import { WikidataService } from '../wikidata.service';
import { EditItemData } from '../model/edit-item-data';
import { PauseService } from '../pause.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit {
  item: Entity;

  editItemData: EditItemData;

  oldRevision: Entity;

  edit: Edit;

  paused: boolean;

  relatedEntities: Entity;

  constructor(
    private pauseService: PauseService,
    private wikidataService: WikidataService,
  ) {}

  ngOnInit() {
    this.paused = this.pauseService.paused;
    this.pauseService.pausedSubject.subscribe(value => {
      this.paused = value;
    });

    this.wikidataService.itemStream().subscribe(editItemData => {
      this.editItemData = editItemData;
      this.item = editItemData.item;
      this.oldRevision = editItemData.oldRevision;
      this.edit = editItemData.edit;
      this.relatedEntities = editItemData.relatedEntities;
    });
  }
}
