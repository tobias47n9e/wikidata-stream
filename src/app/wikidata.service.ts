import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import * as wdk from 'wikidata-sdk';
import { ClaimSnakEntityValue, Entity } from 'wikidata-sdk';
import { HttpClient } from '@angular/common/http';
import { EntityResponse } from './model/entity-response';
import { Dictionary } from 'wikidata-sdk/types/helper';
import { StreamService } from './stream.service';
import * as moment from 'moment';
import { PauseService } from './pause.service';
import { Edit } from './model/edit';
import { EditItemData } from './model/edit-item-data';
import { concatAll, filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WikidataService {
  public propertyEntities: Dictionary<Entity> = {};

  private lastUpdate: moment.Moment;

  private paused: boolean;

  private wikidataEntityBase =
    'https://www.wikidata.org/wiki/Special:EntityData';

  private updateInterval: number = 5;

  constructor(
    private streamService: StreamService,
    private client: HttpClient,
    private pauseService: PauseService,
  ) {}

  itemStream(): Observable<EditItemData> {
    const currentTime = moment();
    this.lastUpdate = currentTime.subtract({ seconds: 5 });
    this.paused = this.pauseService.paused;
    this.pauseService.pausedSubject.subscribe(value => {
      this.paused = value;
    });

    return this.streamService.observeStream().pipe(
      filter((edit: Edit) => {
        return (
          edit.wiki === 'wikidatawiki' &&
          edit.type === 'edit' &&
          !!edit.title.match(/[QLP]\d+/)
        );
      }),
      filter(() => {
        const eventTime = moment();
        const timeDifference = moment.duration(eventTime.diff(this.lastUpdate));

        return (
          this.paused === false &&
          timeDifference.asSeconds() > this.updateInterval
        );
      }),
      map((edit: Edit) => {
        this.lastUpdate = moment();
        return this.fetchItemsForEdit(edit);
      }),
      concatAll(),
    );
  }

  fetchSingleItem(entityId: string): Observable<EntityResponse> {
    const url = wdk.getEntities({ ids: [entityId] });
    return this.client.get<EntityResponse>(url);
  }

  fetchSingleItemEntity(entityId: string): Observable<Entity> {
    const url = wdk.getEntities({ ids: [entityId] });
    return this.client.get<EntityResponse>(url).pipe(
      map(entityResponse => {
        return entityResponse.entities[entityId];
      }),
    );
  }

  fetchSingleItemAtRevision(
    entityId: string,
    revision: number,
  ): Observable<EntityResponse> {
    const params = new URLSearchParams();
    params.set('revision', revision.toString());
    const url = `${
      this.wikidataEntityBase
    }/${entityId}.json?${params.toString()}`;
    return this.client.get<EntityResponse>(url);
  }

  fetchItemsForEdit(edit: Edit): Observable<EditItemData> {
    if (edit.revision.old) {
      return forkJoin([
        this.fetchSingleItem(edit.title),
        this.fetchSingleItemAtRevision(edit.title, edit.revision.old),
      ]).pipe(
        map(result => {
          const item = result[0].entities[edit.title];
          const oldItem = result[1].entities[edit.title];
          const propertyKeys = Object.keys(item.claims).concat(
            Object.keys(oldItem.claims),
          );

          for (const propertyKey of Object.keys(item.claims)) {
            const propertyStatements = item.claims[propertyKey];
            for (const statement of propertyStatements) {
              if (
                statement.mainsnak.datavalue &&
                statement.mainsnak.datavalue.type &&
                statement.mainsnak.datavalue.type === 'wikibase-entityid'
              ) {
                const itemValue = <ClaimSnakEntityValue>(
                  statement.mainsnak.datavalue
                );
                propertyKeys.push(itemValue.value.id);
              }
            }
          }

          const propertyRequests = propertyKeys.map(propertyKey => {
            return this.fetchSingleItemEntity(propertyKey);
          });

          return forkJoin(propertyRequests).pipe(
            map((result: Entity[]) => {
              return <EditItemData>{
                edit: edit,
                item: item,
                oldRevision: oldItem,
                relatedEntities: result.reduce((dictionary, entity: Entity) => {
                  dictionary[entity.title] = entity;
                  return dictionary;
                }, {}),
              };
            }),
          );
        }),
        concatAll(),
      );
    }

    return this.fetchSingleItem(edit.title).pipe(
      map(entityResponse => {
        const item = entityResponse.entities[edit.title];
        const propertyKeys = Object.keys(item.claims);
        const propertyRequests = propertyKeys.map(propertyKey => {
          return this.fetchSingleItemEntity(propertyKey);
        });

        return forkJoin(propertyRequests).pipe(
          map((result: Entity[]) => {
            return <EditItemData>{
              edit: edit,
              item: item,
              relatedEntities: result.reduce((dictionary, entity: Entity) => {
                dictionary[entity.title] = entity;
                return dictionary;
              }, {}),
            };
          }),
        );
      }),
      concatAll(),
    );
  }
}
