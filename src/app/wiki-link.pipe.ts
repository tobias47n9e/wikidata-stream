import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "wikiLink"
})
export class WikiLinkPipe implements PipeTransform {
  transform(value: string, args?: any): any {
    return value.replace(
      /href=\"\/wiki/g,
      'href="https://www.wikidata.org/wiki'
    );
  }
}
