import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "targetBlank"
})
export class TargetBlankPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    return value.replace(/href=/g, 'target="_blank" href=');
  }
}
