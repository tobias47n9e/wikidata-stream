import { Component, OnInit } from '@angular/core';
import {AngularFluentService} from "angular-fluent";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'wikidata-stream';

  constructor(private angularFluentService: AngularFluentService) {}

  ngOnInit(): void {
    this.angularFluentService.setLocale("en");
    this.angularFluentService.setFallbackLocales(["en"]);
  }
}
