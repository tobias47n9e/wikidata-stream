import { Dictionary } from "wikidata-sdk/types/helper";
import { Entity } from "wikidata-sdk";

export interface EntityResponse {
  entities: Dictionary<Entity>;
}
