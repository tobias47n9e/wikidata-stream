import { Compare } from "./compare";

export interface CompareResponse {
  compare: Compare;
}
