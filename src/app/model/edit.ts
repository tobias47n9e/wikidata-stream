import { EditLength } from "./edit-length";
import { RevisionChange } from "./revision-change";

export interface Edit {
  bot: boolean;
  comment: string;
  id: number;
  length?: EditLength;
  meta: any;
  minor: boolean;
  namespace: number;
  parsedcomment: string;
  patrolled: boolean;
  revision: RevisionChange;
  server_name: string;
  server_script_path: string;
  server_url: string;
  timestamp: number;
  title: string; // Wikidata ID or page title
  type: string;
  user: string;
  wiki: string;
}
