import { Edit } from './edit';
import { Entity } from 'wikidata-sdk';

export interface EditItemData {
  edit: Edit;
  item: Entity;
  oldRevision?: Entity;
  relatedEntities?: Entity;
}
