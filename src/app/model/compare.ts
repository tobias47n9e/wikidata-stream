export interface Compare {
  "*": string;
  fromid: number;
  fromns: number;
  fromrevid: number;
  fromtitle: string;
  toid: number;
  tons: number;
  torevid: number;
  tottitle: string;
}
