export interface EditLength {
  new: number;
  old: number;
}
