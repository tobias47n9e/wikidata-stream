import { LocaleString } from "./locale-string";

export interface UpdatedLocaleString {
  old: LocaleString;
  new: LocaleString;
}
