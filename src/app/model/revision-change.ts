export interface RevisionChange {
  new: number;
  old?: number; // New items don't have an old revision
}
