export interface LocaleString {
  language: string;
  value: string;
}
