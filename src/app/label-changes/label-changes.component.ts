import { Component, Input, OnInit } from '@angular/core';
import { Entity } from 'wikidata-sdk';
import { AngularFluentService } from 'angular-fluent';
import * as jsdiff from 'diff';

@Component({
  selector: 'app-label-changes',
  templateUrl: './label-changes.component.html',
  styleUrls: ['./label-changes.component.scss'],
})
export class LabelChangesComponent implements OnInit {
  @Input()
  item: Entity;

  @Input()
  oldRevision: Entity;

  get labelLocales(): string[] {
    return Object.keys(this.item.labels).concat(
      Object.keys(this.oldRevision.labels),
    );
  }

  labelDiffs(locale: string) {
    let oldLabel = '';
    let newLabel = '';

    if (this.oldRevision.labels[locale]) {
      oldLabel = this.oldRevision.labels[locale].value;
    }

    if (this.item.labels[locale]) {
      newLabel = this.item.labels[locale].value;
    }

    return jsdiff.diffChars(oldLabel, newLabel).filter(diff => {
      return diff.added || diff.removed;
    });
  }

  constructor() {}

  ngOnInit() {}
}
