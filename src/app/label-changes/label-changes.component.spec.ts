import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabelChangesComponent } from './label-changes.component';

describe('LabelChangesComponent', () => {
  let component: LabelChangesComponent;
  let fixture: ComponentFixture<LabelChangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LabelChangesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
