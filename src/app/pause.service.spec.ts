import { TestBed } from '@angular/core/testing';

import { PauseService } from './pause.service';

describe('PauseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PauseService = TestBed.get(PauseService);
    expect(service).toBeTruthy();
  });
});
