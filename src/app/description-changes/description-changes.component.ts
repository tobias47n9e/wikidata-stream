import { Component, Input, OnInit } from '@angular/core';
import { Entity } from 'wikidata-sdk';
import * as jsdiff from 'diff';

@Component({
  selector: 'app-description-changes',
  templateUrl: './description-changes.component.html',
  styleUrls: ['./description-changes.component.scss'],
})
export class DescriptionChangesComponent implements OnInit {
  @Input()
  item: Entity;

  @Input()
  oldRevision: Entity;

  get descriptionLocales(): string[] {
    return Object.keys(this.item.descriptions).concat(
      Object.keys(this.oldRevision.descriptions),
    );
  }

  descriptionDiffs(locale: string) {
    let oldDescription = '';
    let newDescription = '';

    if (this.oldRevision.descriptions[locale]) {
      oldDescription = this.oldRevision.descriptions[locale].value;
    }

    if (this.item.descriptions[locale]) {
      newDescription = this.item.descriptions[locale].value;
    }

    return jsdiff
      .diffChars(oldDescription, newDescription)
      .filter(diff => diff.added || diff.removed);
  }

  constructor() {}

  ngOnInit() {}
}
