import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptionChangesComponent } from './description-changes.component';

describe('DescriptionChangesComponent', () => {
  let component: DescriptionChangesComponent;
  let fixture: ComponentFixture<DescriptionChangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DescriptionChangesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
