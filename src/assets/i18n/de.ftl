watch-stream = Verfolge aktuelle Wikidata-Bearbeitungen
wikidata-stream = Wikidata Stream
settings = Einstellungen
built-at = Gebaut beim
source-code = Quellcode
apply-and-close = Speichern & Schließen
locale = Sprache
missing = Fehlt
